CREATE DATABASE main;

CREATE TABLE orders (
order_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
order_email TEXT NOT NULL,
order_dates DATE NOT NULL
);

CREATE TABLE users (
id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
name TEXT NOT NULL,
email TEXT NOT NULL UNIQUE KEY
);

ALTER TABLE orders ADD CONSTRAINT FKey FOREIGN KEY (order_email) REFERENCES users(email) ON UPDATE CASCADE ON DELETE CASCADE;
//������� ������� �� ���������� ������ �� ��������

INSERT INTO users (id, NAME, email) VALUES
(1, 'User 1', 'qwer1@mail.ru'),
(2, 'User 2', 'qwer2@mail.ru'),
(3, 'User 3', 'qwer3@mail.ru');

INSERT INTO orders (order_id, order_email, order_dates) VALUES
(1, 'qwer1@mail.ru', '2019-12-10'),
(2, 'qwer2@mail.ru', '2019-12-11'),
(3, 'qwer3@mail.ru', '2019-12-12');
